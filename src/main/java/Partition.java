import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Partition {

	public static void main(String[] args){
		String a[] = new String[]{"abc","klm","xyz","pqr"};
		List list1 = Arrays.asList(a);

		List t = partition(list1, 1);
		System.out.println(t);

	}

	public static <T> List<List<T>> partition(List<T> list, int taille){
		try {
			controleTaille(taille);
			controleList(list);

			int nbrePartition = list.size() / taille;
			if(list.size() % taille != 0){
				nbrePartition ++;
			}

			List<List<T>> resultat = new ArrayList<List<T>>(nbrePartition);
			for(int i=0; i<nbrePartition; i++){
				resultat.add(new ArrayList<T>());
			}

			for(int i=0; i<list.size(); i++){
				int index = i / taille;
				resultat.get(index).add(list.get(i));
			}

			return resultat;
		} catch (DonneesErroneeException e) {
			System.out.println("Error");
		}
		return null;
	}

	public static <T> void controleList(List<T> list) throws DonneesErroneeException {
		if (list == null)
			throw new DonneesErroneeException("Votre liste est null");
	}

	public static void controleTaille(int taille) throws DonneesErroneeException {
		if (taille <= 0)
			throw new DonneesErroneeException("La taille renseignée est inférieure ou égale à 0");
	}
}

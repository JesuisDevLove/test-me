public class DonneesErroneeException  extends Exception {
	public DonneesErroneeException() {
		super();
	}

	public DonneesErroneeException(String s) {
		super(s);
	}
}

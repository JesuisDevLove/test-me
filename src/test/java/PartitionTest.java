import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PartitionTest {

	@Test
	public void testPartitionEmpty() {
		assertEquals(Arrays.<List<Object>>asList(), Partition.partition(Arrays.asList(), 3));
	}

	@Test
	public void testPartitionStrings() {
		assertEquals(
				Arrays.asList(
						Arrays.asList("1", "2"),
						Arrays.asList("3", "4"),
						Arrays.asList("5")),
				Partition.partition(Arrays.asList("1", "2", "3", "4", "5"), 2));
	}

	@Test
	public void testPartitionInts() {
		assertEquals(
				Arrays.asList(
						Arrays.asList(1, 2),
						Arrays.asList(3, 4),
						Arrays.asList(5)),
				Partition.partition(Arrays.asList(1, 2, 3, 4, 5), 2));
	}

	@Test
	public void testPartition_MultipleOfSize() {
		assertEquals(
				Arrays.asList(
						Arrays.asList(1, 2),
						Arrays.asList(3, 4)),
				Partition.partition(Arrays.asList(1, 2, 3, 4), 2));
	}


	@Test
	public void testPartition_Size1() {
		assertEquals(
				Arrays.asList(
						Arrays.asList(1),
						Arrays.asList(2),
						Arrays.asList(3),
						Arrays.asList(4)),
				Partition.partition(Arrays.asList(1, 2, 3, 4), 1));
	}

	@Test
	public void testPartition_BiggerSize() {
		List<Integer> orig = Arrays.asList(1, 2, 3, 4);
		assertEquals(
				Arrays.asList(orig),
				Partition.partition(orig, orig.size() + 11));
	}
}